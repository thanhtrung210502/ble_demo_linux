cmake_minimum_required(VERSION 3.21)

include(${CMAKE_CURRENT_SOURCE_DIR}/SimpleBLE/cmake/prelude.cmake)
project(ble_demo)
include(${CMAKE_CURRENT_SOURCE_DIR}/SimpleBLE/cmake/epilogue.cmake)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_EXTENSIONS OFF)

option(SIMPLEBLE_LOCAL "Use local SimpleBLE" ON)

if (SIMPLEBLE_LOCAL)
    add_subdirectory(${CMAKE_CURRENT_LIST_DIR}/SimpleBLE/simpleble ${CMAKE_BINARY_DIR}/simpleble)
else()
    find_package(simpleble CONFIG REQUIRED)
endif()

add_executable(ble_demo
    main.cpp
    SimpleBLE/examples/simpleble/cpp/common/utils.cpp
)

target_link_libraries(ble_demo simpleble::simpleble)

