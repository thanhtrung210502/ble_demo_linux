## Git clone
git clone --recursive https://gitlab.com/thanhtrung210502/ble_demo_linux.git

## Enviroment
- Install latest **Cmake** (**later than 3.21**)
- Install **libdbus-1-dev** with command:
> sudo apt update
>
> sudo apt install libdbus-1-dev

## Build
- Change directory to the root folder of this repo
- Run these command: 
> mkdir build && cd build
>
> cmake .. 
>
> cmake --build .

# Binary
After building successfully, binary demo will be in the **bin** folder of **build** folder

# How to demo
- Make sure the **Nordic kit** is downloaded with to **Nordic Blinky** application
- Run **ble_demo** in **bin** folder
- Follow the guideline on this application to demo

## Next Development
This demo is used SimpleBLE library, that is a open source library related to BLE in linux, this is the official source code and documents of this library
> https://github.com/OpenBluetoothToolbox/SimpleBLE
>
> https://simpleble.readthedocs.io/en/latest/

In the documents, it's show how to build some sample for BLE. Just follow and try

And please read the sample code to explore how to use some useful APIs of this library 
