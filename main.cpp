#include <chrono>
#include <iostream>
#include <thread>
#include <vector>

#include "SimpleBLE/examples/simpleble/cpp/common/utils.hpp"

#include "simpleble/SimpleBLE.h"

using namespace std::chrono_literals;

constexpr char device_name[] {"Nordic_Blinky"};
constexpr char blinky_service[] {"00001523-1212-efde-1523-785feabcd123"};
constexpr char button_characteristics[] {"0001524-1212-efde-1523-785feabcd123"};
constexpr char led_characteristics[] {"00001525-1212-efde-1523-785feabcd123"};

const auto menu {
    "\n"
    "Menu: \n"
    "\t1. Read button 1 status\n"
    "\t2. Turn on  led 3\n"
    "\t3. Turn off led 3\n"
    "\t4. Exit\n"
    "\n"
    "Enter your choice: "
};

int main() {
    std::cout << "************ BLE Applicatioin started ************\n";
    auto adapter_optional = Utils::getAdapter();

    if (!adapter_optional.has_value()) {
        return EXIT_FAILURE;
    }

    auto adapter = adapter_optional.value();

    std::vector<SimpleBLE::Peripheral> peripherals;

    adapter.set_callback_on_scan_found([&](SimpleBLE::Peripheral peripheral) {
        // std::cout << "Found device: " << peripheral.identifier() << " [" << peripheral.address() << "]" << std::endl;
        peripherals.push_back(peripheral);
    });

    adapter.set_callback_on_scan_start([]() { std::cout << "Scan started." << std::endl; });
    adapter.set_callback_on_scan_stop([]() { std::cout << "Scan stopped." << std::endl; });
    // Scan for 3 seconds and return.
    adapter.scan_for(3000);

    auto peripheral = std::find_if(peripherals.begin(), peripherals.end(), [](auto p){ return p.identifier() == device_name;});
    if (peripheral  != peripherals.end())
    {
        std::cout << "Connecting to " << peripheral->identifier() << " [" << peripheral->address() << "]" << std::endl;
        peripheral->connect();
        std::cout << "Successfully connected" << std::endl;
        // Store all service and characteristic uuids in a vector.
        std::vector<std::pair<SimpleBLE::BluetoothUUID, SimpleBLE::BluetoothUUID>> uuids;
        for (auto service : peripheral->services()) {
            for (auto characteristic : service.characteristics()) {
                uuids.push_back(std::make_pair(service.uuid(), characteristic.uuid()));
            }
        }

        bool flag_continue = true;
        while(flag_continue)
        {
            std::cout << menu;
            
            int choice = 0;
            std::cin >> choice;
            switch(choice)
            {
                case 1:
                {
                    // Find button characteristics 
                    auto button_uuid = std::find_if(uuids.begin(), uuids.end(), [](auto u) {
                        return u.second.compare(button_characteristics);
                    });

                    if (button_uuid != uuids.end())
                    {
                        std::cout << "Press and release button 1. The status of button 1 will display here\n";
                        std::cout << "Enter 'q' to exit\n";
                        // Enable notify button characteristics
                        peripheral->notify(button_uuid->first, button_uuid->second, [&](SimpleBLE::ByteArray bytes) {
                            if (bytes[0] == 1)
                            {
                                std::cout << "Button 1 is pressed\n";
                            }
                            else if (bytes[0] == 0)
                            {
                                std::cout << "Button 1 is released\n";
                            }
                            else
                            {
                                std::cout << "Error. The data is " << bytes << std::endl;
                            }
                            });
                        
                        while(true)
                        {
                            std::string temp;
                            std::cin >> temp;
                            if (temp == "q")
                            {
                                break;
                            }
                            std::this_thread::sleep_for(1s);
                        }
                    }
                    else
                    {
                        std::cout << "Can't find button characteristics\n";
                    }

                    break;
                }

                case 2:
                {
                    std::cout << "Turn on led\n";
                    std::string contents {"1"};
                    contents[0] -= 0x30;
                    peripheral->write_request(uuids[1].first, uuids[1].second, contents);
                    break;
                }
                case 3:
                {
                    std::cout << "Turn off led\n";
                    std::string contents {"0"};
                    contents[0] -= 0x30;
                    peripheral->write_request(uuids[1].first, uuids[1].second, contents);
                    break;
                }
                case 4:
                {
                    flag_continue = false;
                    if (peripheral->is_connected())
                    {
                        peripheral->disconnect();
                    }
                    break;
                }
                default:
                {
                    break;            
                }
            }


            std::this_thread::sleep_for(2s);
        }
    }
    else
    {
        std::cout << "Can't find " << device_name << "\n";
    }

    return EXIT_SUCCESS;
}
